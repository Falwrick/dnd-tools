class character:
    def __init__(self,name, strength, dexterity, constitution, intelligence, wisdom,
            charisma):
        self.name = name
        self.strength = strength
        self.dexterity = dexterity
        self.constitution = constitution
        self.intelligence = intelligence
        self.wisdom = wisdom
        self.charisma = charisma

def mod(stat):
    mod = (stat - 10) // 2
    return(mod)
