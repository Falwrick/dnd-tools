import pickle
from character import character
from character import mod
import diceRoller
import os
#This part is making a new character or loading up the last one 
if os.path.isfile("sheet") == True:
    loading = True
    while loading == True:
        new = input("Would you like to load your last character? ")
        if new.lower() == "no":
            sheet = open("sheet", "wb")
            name = input("What is your character's name?")
            player = character(name,
                    input("str?"),input("dex?"),input("con?"),input("int?"),input("wis?"),input("chr?"))
            pickle.dump(player, sheet)
            print("Welcome, " + player.name)
            loading = False
        elif new.lower() == "yes":
            sheet = open("sheet", "rb")
            player = pickle.load(sheet)
            print("Welcome back, " + player.name)
            loading = False
        else:
            print("That is not a valid answer, please say yes or no")
#If no save file is found, the player is set to automatically make one
else:
    sheet = open("sheet", "wb")
    name = input("What is your character's name?")
    player = character(name,
            input("str?"),input("dex?"),input("con?"),input("int?"),input("wis?"),input("cha?"))
    pickle.dump(player, sheet)
    print("Welcome, " + player.name)
#Setting up keys so they don't need to type out the entire name
shortStats = {"str":"strength" , "dex":"dexterity", "con":"constitution",
        "int":"intelligence", "wis":"wisdom", "cha":"charisma"}
#This is the main run, set up to roll an attribute saving throw
playing = True
while playing == True:
    choice = input("Type in a command, or type ? for help ")
    if choice.lower() == "quit" or choice.lower() == "exit":
        playing = False
    elif choice == "?" or choice.lower() == "help":
        print("Pick a stat to roll [str, dex, con, int, wis, cha]")
        print("Enter c for a custom roll")
        print("Type adv or dis for advantage and disadvantage rolls")
        print("Type quit if you would like to quit")
    elif choice in shortStats:
        choice = shortStats[choice]
        print(diceRoller.roll(20, mod(int(getattr(player, choice)))))
    elif choice.lower() == "c":
        print(diceRoller.roll(int(input("How many sides? ")) , int(getattr(player,(shortStats[input("Which mod? ")]))) , int(input("How many rolls? "))))
    elif choice.lower() == "adv":
        print(diceRoller.advRoll(mod(int(getattr(player, shortStats[input("Which stat are you rolling for?")])))))
    elif choice.lower() == "dis":
        print(diceRoller.disRoll(mod(int(getattr(player, shortStats[input("Which stat are you rolling for?")])))))
    else:
        print("That is not a valid choice!")
print("See you later, " + player.name)
