import random
#Defining the die aspect of the game
def roll(side, mod, rolls=1):
    total = 0
    while rolls != 0:
        face = random.randint(1, side)
        print("You rolled a " + str(face))
        if (face) == side:
            print("Critical Success!")
        elif (face) == 1:
            print("Critical Fail!")
        total = total + face
        rolls = rolls - 1
    total = total + mod
    return(total)

def advRoll(mod):
    results = [random.randint(1, 20), random.randint(1, 20)]
    print("You rolled a " + str(results[0])  + " and a " + str(results[1]))
    if max(results) == 20:
        print("Critical Sucess!")
    total = max(results) + int(mod)
    return(total)
def disRoll(mod):
    results = [random.randint(1, 20), random.randint(1, 20)]
    print("You rolled a " + str(results[0])  + " and a " + str(results[1]))
    if min(results) == 1:
        print("Critical Fail!")
    total = min(results) + int(mod)
    return(total)
print(disRoll(0))
